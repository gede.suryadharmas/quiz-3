<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index(){
        return view ('welcome');
    }
    public function buku(){
        return view ('buku');
    }
    public function create(){
        return view ('create');
    }
    public function edit(){
        return view ('edit');
    }
}
