<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PagesController@gambar');
Route::get('/buku', 'PagesController@buku');
Route::get('/buku/create', 'PagesController@bukubaru');
Route::post('/buku', 'PagesController@postbuku');
Route::get('/buku/{id}', 'PagesController@detailbuku');
Route::get('/buku/{id}/edit', 'PagesController@editbuku');
Route::put('/buku/{id}', 'PagesController@simpanbuku');
Route::delete('/buku/{id}', 'PagesController@hapusbuku');