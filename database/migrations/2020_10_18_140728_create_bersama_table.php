<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBersamaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bersama', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();

            $table->foreign('buku_id')->references('id')->on('buku');
            $table->foreign('penulis_id')->references('id')->on('penulis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bersama');
        $table->dropForeign(['buku_id']);
        $table->dropColumn(['buku_id']);
        $table->dropForeign(['penulis_id']);
        $table->dropColumn(['penulis_id']);
    }
}
